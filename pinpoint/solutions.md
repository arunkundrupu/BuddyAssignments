Pinpoint:

- Pinpoint is an appliction performance Management tool for large scale distributed systems written in Java.
- Pinpoint provides a solution to help analyze the overall structure of the system and how components with in them are interconnected by tracing transactions acrossdistributed applications.

Pinpoint setup using Docker:

$ git clone https://github.com/naver/pinpoint-docker.git

$ cd pinpoint-docker

or pull docker image

$ docker search pinpoint-hbase

![docker](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/pinpoint/images/01\_p\_doxker\_search.png)

$ docker-compose pull &amp;&amp; docker-compose up -d

![docker](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/pinpoint/images/02\_p\_docker-com\_pull\_up.png)

$ docker ps (will list the running containers)

![]docker](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/pinpoint/images/03\_p\_docker\_ps.png)

Register a job on pinpoint-flink server

http://192.168.0.88:8081

![docker](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/pinpoint/images/04\_p\_flink-server.png)

create the job and upload the jar file

![docker](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/pinpoint/images/05\_p\_job\_submission.png)

now access the pinpoint web page

http://192.168.0.88:8079

# select the application and monitor

![docker](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/pinpoint/images/07\_p\_quick\_app.png)
