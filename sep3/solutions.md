AWS Code Deploy:

Created a jenkins job and used the springhibernate3 code for code deploy.

![AwsCD](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/sep3-images/01.png)

![AwsCD](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/sep3-images/02.png)

Configured the deploy an application to aws code deploy under post-build actions

![AwsCD](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/sep3-images/03.png)]

Used code deploy service in aws and created an application.

Run the build in jenkins

![AwsCD](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/sep3-images/05.png)

Deployment has be created with revision d-X7Y54X9ST

![AwsCD](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/sep3-images/06.png)

![AwsCD](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/sep3-images/07.png)
