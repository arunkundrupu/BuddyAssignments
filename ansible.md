

Ansible:

- Ansible is a simple open IT automation and configuration management tool which can be used to automate application deployment and cloud provisioning.
- Ansible uses playbook to describe automation jobs and playbook uses very simple langauge called YAML.
- Ansible uses &#39;push&#39; mechanism.
- Ansible works by connecting your nodes and pushing out small programs called ansible modules, and removes the modules after installation.
- Ansible server also called as controlling node connects the nodes through SSH
- It is designed for multi-tier deployment
- It is completely agentless.

Ansible vs other configuration management tools:

1) Availability:

All the tools are highly available which means that there are multiple servers or multiple instance present. If the main master or server goes down, there is always a backup server or the different master to take its place.

Ansible  : It runs with a single active node, called the Primary instance. If primary goes down, there is a Secondary instance to take its place.

Chef      : When there is a failure on the primary server i.e. chef server, it has a backup server to take the place of the primary server.

Puppet  :  It has multi-master architecture, if the active master goes down, the other master takes the active master place.

Saltstack: It can have multiple masters configured. If one master is down, agents connect with the other master in the list. Therefore it has multiple masters to configure salt minions.

### **2) Ease of Setup:**

**Ansible   : It has only master running on the server machine, but no agents running on the client machine. It uses** _ssh_ **connection to login to client systems or the nodes you want to configure. Client machine VM requires no special setup, hence it is faster to setup.**

**Chef  :** Chef has a master-agent architecture. _Chef server_ runs on the master machine and Chef client runs as an agent on each client machine. Also, there is an extra component called workstation, which contains all the configurations which are tested and then pushed to central chef server.

**Puppet** : Puppet also has a master-agent architecture. _Puppet server_ runs on the master machine and _Puppet clients_ runs as an agent on each client machine. After that, there is also a certificate signing between the agent and the master.

**Saltstack** : Here Server is called as salt _master_ and clients are called as salt _minions_ which run as agents in the client machine.


