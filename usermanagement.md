

LINUX USER MANAGEMENT:

Since Linux is a multi-user operating system (in that it allows multiple users on different computers or terminals to access a single system), you will need to know how to perform effective user management: how to add, edit, suspend, or delete user accounts, along with granting them the necessary permissions to do their assigned tasks.

- To add a new user account :

# adduser [new\_account]

# useradd [new\_account]

When a new user account is added to the system, the following operations are performed.

- His/her home directory is created ( **/home/username** by default).
- The following hidden files are copied into the user&#39;s home directory, and will be used to provide environment variables for his/her user session. (.bash\_logout , .bash\_profile, &amp; .bashrc)
- A mail spool is created for the user at /var/spool/mail/ **username**.
- A group is created and given the same name as the new user account.

​Understanding /etc/passwd:

The full account information is stored in the **/etc/passwd** file. This file contains a record per system user account and has the following format (fields are delimited by a colon).

[username]:[x]:[UID]:[GID]:[Comment]:[Home directory]:[Default shell]

- Fields **[user**name] and [Comment] are self explanatory.

- The x in the second field indicates that the account is protected by a shadowed password (in /etc/shadow), which is needed to logon as [username].

- The [UID] and [GID] fields are integers that represent the User IDentification and the primary Group IDentification to which [username] belongs, respectively.

- The [Home directory] indicates the absolute path to [username]&#39;s home directory, and

- The [Default shell] is the shell that will be made available to this user when he or she logins the system

​Understanding /etc/group

Group information is stored in the **/etc/group** file. Each record has the following format.

[Group name]:[Group password]:[GID]:[Group members]

1. [Group name] is the name of group.
2. An x in [Group password] indicates group passwords are not being used.
3. [GID]: same as in /etc/passwd.
4. [Group members]: a comma separated list of users who are members of [Group name].





1.
# ​Access Control Lists(ACL) in Linux

**ACL:**
Access control list (ACL) provides an additional, more flexible permission mechanism for file systems. It is designed to assist with UNIX file permissions. ACL allows you to give permissions for any user or group to any disc resource.

**Use of ACL :**
Think of a scenario in which a particular user is not a member of group created by you but still you want to give some read or write access, how can you do it without making user a member of group, here comes in picture Access Control Lists, ACL helps us to do this trick.

Basically, ACLs are used to make a flexible permission mechanism in Linux.

From Linux man pages, ACLs are used to define more fine-grained discretionary access rights for files and directories.

**setfacl** and **getfacl** are used for setting up ACL and showing ACL respectively.

# getfacl &lt;/path of file or directory&gt;

**List of commands for setting up ACL :**

1) To add permission for user

**setfacl -m &quot;u:user:permissions&quot; /path/to/file**

2) To add permissions for a group

**setfacl -m &quot;g:group:permissions&quot; /path/to/file**

3) To allow all files or directories to inherit ACL entries from the directory it is within

**setfacl -dm &quot;entry&quot; /path/to/dir**

4) To remove a specific entry

**setfacl -x &quot;entry&quot; /path/to/file**

5) To remove all entries

**setfacl -b path/to/file**

**Modifying ACL using setfacl :**
To add permissions for a user (user is either the user name or ID):

# setfacl -m &quot;u:user:permissions&quot;

To add permissions for a group (group is either the group name or ID):

# setfacl -m &quot;g:group:permissions&quot;

To allow all files or directories to inherit ACL entries from the directory it is within:

# setfacl -dm &quot;entry&quot;

**Remove ACL :**
If you want to remove the set ACL permissions, use setfacl command with -b option.

