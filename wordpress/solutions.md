

**Assignment: Configure WordPress**

**WordPress:**

- WordPress is free open source web publishing system.
- WordPress is a popular content management system

Configuring WordPress with LAMP setup:

WordPress need a web server, a database, and PHP in order to correctly function.

1. Apache2:

sudo apt-get install apache2 apache2-utils

$ sudo systemctl enable apache2

$ sudo systemctl start apache2

![wordpress](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/wordpress/01%20WP-apache.png)

2. MySQL:
Create a MySQL Database and User for WordPress:

$ sudo apt-get install mysql-client mysql-server

set the root user password for mysql

The database server deployment is not yet secure so

$ sudo mysql\_secure\_installation

validate\_password

 **3. Install PHP and Modules:**

Install PHP and a few modules for it to work with the web and database servers

$ sudo apt-get install php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-cli php7.0-cgi php7.0-gd

if php is working in collaboration with the web server, we need to create a info.php file inside /var/www/html

$ sudo vi /var/www/html/info.php

\&lt;?php

phpinfo();

?\&gt;

to access the server http://192.168.0.88/info.php

![wordpress](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/wordpress/02%20WP-php.png)

 **4. Install WordPress CMS**

$ sudo wget -c http://wordpress.org/latest.tar.gz

$ Sudo tar -xzvf latest.tar.gz

move the WordPress files from the extracted folder to the Apache default root directory, /var/www/html/

$ sudo rsync -av wordpress/\* /var/www/html/

set the correct permissions on the website directory, i.e. give ownership of the WordPress files to the web server

$ sudo chown -R www-data:www-data /var/www/html/

$ sudo chmod -R 755 /var/www/html/

 **5. Create WordPress Database:**

$ mysql -u root -p

mysql\&gt; CREATE DATABASE wp\_myblog;

mysql\&gt; GRANT ALL PRIVILEGES ON wp\_myblog.\* TO &#39;arun&#39;@&#39;localhost&#39; IDENTIFIED BY &#39;password&#39;;

mysql\&gt; FLUSH PRIVILEGES;

mysql\&gt; EXIT;

under /var/www/html/ directory and rename existing wp-config-sample.php to wp-config.php

$ sudo mv wp-config-sample.php wp-config.php

update it with your database information under the MySQL settings section

give datebase name, username, db host details.

$ sudo systemctl restart apache2.service

$ sudo systemctl restart mysql.service

http://192.168.0.88/wp-admin

![wordpress](https://github.com/arunkundrupu1990/BuddyAssignments/blob/master/wordpress/04%20WP-dashboard.png)


