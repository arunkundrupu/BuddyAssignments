

**Linux file system:**

everything you encounter on a Linux system is a file, there are some exceptions.

- _Directories_: files that are lists of other files.

- _Special files_: the mechanism used for input and output. Most special files are in /dev.
- _Links_: a system to make a file or directory visible in multiple parts of the system&#39;s file tree.
- _(Domain) sockets_: a special file type, similar to TCP/IP sockets, providing inter-process networking protected by the file system&#39;s access control.
- _Named pipes_: act more or less like sockets and form a way for processes to communicate with each other, without using network socket semantics.

    1. ​Ext2

- Ext2 stands for second extended file system.
- It was introduced in 1993. Developed by Rémy Card.
- This was developed to overcome the limitation of the original ext file system.
- **Ext2 does not have journaling feature.**
- On flash drives, usb drives, ext2 is recommended, **as it doesn&#39;t need to do the over head of journaling.**
- Maximum individual file size can be from 16 GB to 2 TB
- Overall ext2 file system size can be from 2 TB to 32 TB

 2. ​Ext3

- Ext3 stands for third extended file system.
- It was introduced in 2001. Developed by Stephen Tweedie.
- Starting from Linux Kernel 2.4.15 ext3 was available.
- The main benefit of ext3 is that it **allows journaling.**
- Journaling has a dedicated area in the file system, where all the changes are tracked. **When the system crashes, the possibility of file system corruption is less because of journaling.**
- Maximum individual file size can be from 16 GB to 2 TB
- Overall ext3 file system size can be from 2 TB to 32 TB
- There are three types of journaling available in ext3 file system.
  - Journal – Metadata and content are saved in the journal.
  - Ordered – Only metadata is saved in the journal. Metadata are journaled only after writing the content to disk. This is the default.
  - Writeback – Only metadata is saved in the journal. Metadata might be journaled either before or after the content is written to the disk.
- You can convert a ext2 file system to ext3 file system directly (without backup/restore).

3. ​Ext4

- Ext4 stands for fourth extended file system.
- It was introduced in 2008.
- Starting from Linux Kernel 2.6.19 ext4 was available.
- Supports huge individual file size and overall file system size.
- Maximum individual file size can be from 16 GB to 16 TB
- Overall maximum ext4 file system size is 1 EB (exabyte). 1 EB = 1024 PB (petabyte). 1 PB = 1024 TB (terabyte).
- Directory can contain a maximum of 6,40,000 subdirectories (as opposed to 32,000 in ext3)
- You can also mount an existing ext3 fs as ext4 fs (without having to upgrade it).
- Several other new features are introduced in ext4: **multiblock allocation, delayed allocation, journal checksum.** fast fsck, etc. All you need to know is that these new features have improved the performance and reliability of the filesystem when compared to ext3.
- In ext4, you also have the **option of turning the journaling feature &quot;off&quot;.**

Create an ext2 file system:

mke2fs /dev/sda1

Create an ext2 file system:

mkfs.ext3 /dev/sda1

(or)

mke2fs –j /dev/sda1

Create an ext4 file system:

mkfs.ext4 /dev/sda1

(or)

mke2fs -t ext4 /dev/sda1

1. ​Converting ext2 to ext3

umount /dev/sda2

tune2fs -j /dev/sda2

mount /dev/sda2 /home

2. ​Converting ext3 to ext4

umount /dev/sda2

tune2fs -O extents,uninit\_bg,dir\_index /dev/sda2

e2fsck -pf /dev/sda2

mount /dev/sda2 /home

**File types:**

The -l option to **ls** displays the file type, using the first character of each input line:

| Symbol | Meaning |
| --- | --- |
| - | Regular file |
| d | Directory |
| l | Link |
| c | Special file |
| s | Socket |
| p | Named pipe |
| b | Block device |

When an accident occurs, only the data in the partition that got the hit will be damaged, while the data on the other partitions will most likely survive.

- _data partition_: normal Linux system data, including the _root partition_ containing all the data to start up and run the system; and

- _swap partition_: expansion of the computer&#39;s physical memory, extra memory on hard disk.

On Linux, you will virtually never see irritating messages like _Out of memory, please close some applications first and try again_, because of this extra memory.

Most Linux systems use **fdisk** at installation time to set the partition type.

- a partition for user programs (_/usr_)

- a partition containing the users&#39; personal data (_/home_)
- a partition to store temporary data like print- and mail-queues (_/var_)
- a partition for third party and extra software (_/opt_)

Once the partitions are made, you can only add more. Changing sizes or properties of existing partitions is possible but not advisable.

All partitions(connected to the root partition indicated with /) are attached to the system via a mount point.

On a running system, information about the partitions and their mount points can be displayed using the **df** command (which stands for _disk full_ or _disk free_).

**Subdirectories of the root directory**

| Directory | Content |
| --- | --- |
| /bin | Common programs, shared by the system, the system administrator and the users. |
| /boot | The startup files and the kernel, vmlinuz. In some recent distributions also grub data. Grub is the GRand Unified Boot loader and is an attempt to get rid of the many different boot-loaders we know today. |
| /dev | Contains references to all the CPU peripheral hardware, which are represented as files with special properties. |
| /etc | Most important system configuration files are in /etc, this directory contains data similar to those in the Control Panel in Windows |
| /home | Home directories of the common users. |
| /initrd | (on some distributions) Information for booting. Do not remove! |
| /lib | Library files, includes files for all kinds of programs needed by the system and the users. |
| /lost+found | Every partition has a lost+found in its upper directory. Files that were saved during failures are here. |
| /misc | For miscellaneous purposes. |
| /mnt | Standard mount point for external file systems, e.g. a CD-ROM or a digital camera. |
| /net | Standard mount point for entire remote file systems |
| /opt | Typically contains extra and third party software. |
| /proc | A virtual file system containing information about system resources. More information about the meaning of the files in proc is obtained by entering the command **man**  **proc** in a terminal window. The file proc.txt discusses the virtual file system in detail. |
| /root | The administrative user&#39;s home directory. Mind the difference between /, the root directory and /root, the home directory of the _root_ user. |
| /sbin | Programs for use by the system and the system administrator. |
| /tmp | Temporary space for use by the system, cleaned upon reboot, so don&#39;t use this for saving any work! |
| /usr | Programs, libraries, documentation etc. for all user-related programs. |
| /var | Storage for all variable files and temporary files created by users, such as log files, the mail queue, the print spooler area, space for temporary storage of files downloaded from the Internet, or to keep an image of a CD before burning it. |

**df** command with a dot (.) as an option shows the partition the current directory belongs to, and informs about the amount of space used on this partition

$df -h .

Every partition has its own file system.

In a file system, a file is represented by an _inode_, a kind of serial number containing information about the actual data that makes up the file: to whom this file belongs, and where is it located on the hard disk.

$ ls -li &lt;file name&gt;    (gives inode number of the file)


